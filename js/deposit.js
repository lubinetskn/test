$( document ).ready(function(){
    var valuta, curency='<i class="fa fa-rub"></i>';

    function buildDepos(){
        var render ='';

        for (var prop in deposit_arr) {
            render += '<div class="vklad dib"><div class="vklad-name">'+deposit_arr[prop].title+'</div><div class="item1-content"><span></span>%</div></div>'
        }
        $('#deposit-container').html(render);


        $('.vklad.dib').eq(0).addClass('active');
    };
    buildDepos();

    function vkladCheck(){
        var dep_procent=[];
        for (var prop in deposit_arr) {//проверка внутри тарифа
            var proc='-',
                trueans=0,
                prevsum,
                prevtitle='',
                key_user=[], key_plan=[],
                cl = document.getElementById('oClient').checked,
                pens = document.getElementById('oOld').checked,
                pop = document.getElementById('replenishable').checked,
                cap = document.getElementById('oCapitals').checked,
                esc = document.getElementById('oEsc').checked,
                dep_data = deposit_arr[prop].data, // данные тарифа
                dep_params = deposit_arr[prop].params,
                dop=0; // параметры

            if (cl) {
                if (deposit_arr[prop].honorable_client) {
                    if (valuta == 'рубли') {
                        dop = deposit_arr[prop].honorable_client;
                    }
                    if (valuta == 'доллары') {
                        dop = deposit_arr[prop].honorable_client_dollar;
                    }
                    if (valuta == 'евро') {
                        dop = deposit_arr[prop].honorable_client_euro;
                    }
                }
            }

            if (cl){
                cl=2;
            } else {
                cl=3;
            }
            if (pens){
                pens=2;
            } else {
                pens=3;
            }
            if (pop){
                pop=2;
            } else {
                pop=3;
            }
            if (cap){
                cap=2;
            } else {
                cap=3;
            }
            if (esc){
                esc=2;
            } else {
                esc=3;
            }

            key_user.push(cl,pens,pop,cap,esc);
            for (var parametr in dep_params) {
                key_plan.push(dep_params[parametr]);
            }
            for (var j = 0; j < key_user.length; j++) {
                if ((key_user[j] === key_plan[j]) || (key_plan[j]==1)) {
                    trueans++;
                }
            }

            if (trueans==5){
                for (var check in dep_data) { //проверка внутри данных тарифа
                    if (dep_data[check].srok_max=='-'){
                        dep_data[check].srok_max=500;
                    }
                    if ( dep_data[check].currency === valuta ) {
                        if (( dep_data[check].srok_min <= $('#depositPeriod_input').val() ) && ( dep_data[check].srok_max >= $('#depositPeriod_input').val() )) {
                            if (Number(dep_data[check].sum_from) <= Number($('#depositSum_value').val())) {
                                if (prevtitle == deposit_arr[prop].title) {
                                    if (prevsum) {
                                        if (prevsum < dep_data[check].sum_from) {
                                            console.log("Замена:"+prevsum+'на '+dep_data[check].sum_from);
                                            proc = dep_data[check].procent + dop;
                                            prevsum = dep_data[check].sum_from;
                                        }

                                    } else {
                                        console.log("Замена:"+prevsum+'на '+dep_data[check].sum_from);
                                        proc = dep_data[check].procent + dop;
                                        prevsum = dep_data[check].sum_from;
                                    }
                                } else {
                                    proc = dep_data[check].procent + dop;
                                }
                            }
                        }
                        prevtitle=deposit_arr[prop].title;
                    }

                    console.log("********************************");
                    console.log(deposit_arr[prop].title);
                    console.log("На промежутке:"+dep_data[check].srok_min+' '+dep_data[check].srok_max+'. procent='+proc);
                    console.log("От: "+dep_data[check].sum_from);
                    console.log('prevsum = '+prevsum);
                    console.log('prevtitle = '+prevtitle);
                    console.log("________________________________");
                }
            }
            dep_procent.push(proc);
        }
        console.log(dep_procent);
        for (var i = 0; i < dep_procent.length; i++) {
            $('.vklad.dib').eq(i).find('span').text(dep_procent[i]);
        }
        localStorage.setItem('procents', dep_procent)
    }

    $('#chooseRub').click(function(){
        curency='<i class="fa fa-rub"></i>';
        moveVSumma();
    });

    $('#chooseEuro').click(function(){
        curency='<i class="fa fa-eur"></i>';
        moveVSumma();
    });

    $('#chooseDollar').click(function(){
        curency='<i class="fa fa-usd"></i>';
        moveVSumma();
    });

    var uSlider = document.getElementById('depositPeriod_slider'),
        uSlider_val = document.getElementById('depositPeriod_input');

    noUiSlider.create(uSlider, {
        start: 91,
        range: {
            'min': [0,1],
            'max': [365,1]
        },
        pips: {
            mode: 'values',
            values: [0,91,181,270,365]
        },
        format: wNumb({
            decimals: 0
        })
    });


    var range_ruble_sliders = {
        'min': [     5000 ],
        '20%': [   100000 ],
        '40%': [  1500000 ],
        '60%': [  5000000 ],
        'max': [ 10000000 ]
    };
    var pips_range_ruble_sliders=[5000,100000, 1500000, 5000000, 10000000];

    var updateSlider = document.getElementById('depositSum_slider'),
        updateSliderValue = document.getElementById('depositSum_value'),
        settings = {
            range: range_ruble_sliders,
            start: 5000,
            step: 1,
            format: wNumb({
                decimals: 0
            }),
            pips: {
                mode: 'values',
                values: pips_range_ruble_sliders,
                format: wNumb({
                    thousand: ' '
                })
            }
        };

    function bindValue ( ) {
        updateSlider.noUiSlider.on('update', function( values, handle ) {
            updateSliderValue.value = values[handle];
        });
    }

    noUiSlider.create(updateSlider, settings);
    bindValue();

    var buttons = document.getElementsByClassName('update-button');

    function rebuildSlider ( ) {
        var val = updateSlider.noUiSlider.get(),
            pipsa= this.getAttribute('data-pips').split(',');

        updateSlider.noUiSlider.destroy();

        // Create a slider with the new options.
        settings.start = Number(val);
        settings.range = {
            'min': [Number(pipsa[0])],
            '20%': [Number(pipsa[1])],
            '40%': [Number(pipsa[2])],
            '60%': [Number(pipsa[3])],
            'max': [Number(pipsa[4])]
        };
        settings.pips.values = [Number(pipsa[0]),Number(pipsa[1]),Number(pipsa[2]),Number(pipsa[3]),Number(pipsa[4])];

        noUiSlider.create(updateSlider, settings);

        bindValue();
    }

    buttons[0].addEventListener('click', rebuildSlider);
    buttons[1].addEventListener('click', rebuildSlider);
    buttons[2].addEventListener('click', rebuildSlider);


    function moveVSumma(){
        var srok =uSlider_val.value,
            summa=updateSliderValue.value,
            result,
            stavka=$('.vklad.active').find('.item1-content span').text();

        if($('#chooseDollar').prop('checked')) {
            valuta='доллары';
        }
        if($('#chooseEuro').prop('checked')){
            valuta='евро';
        }
        if($('#chooseRub').prop('checked')){
            valuta='рубли';
        }

        vkladCheck();

        $('.item2').data('procent', stavka); //устанавливаем актуальный процент

        //капитализация вкл/выкл
        if ($('#oCapitals').prop('checked')){
            result=(1+stavka/100*30/365);
            var dop=srok%30;
            srok=Math.floor(srok/30);
            dop=Math.round(summa*stavka*dop/36500);
            result=Math.pow(result,srok);
            result=summa*result-summa+dop;
            result=Math.round(result);
        } else {
            result=Math.round(summa*stavka*srok/36500);
        }

        if ((result!=0) && ( !isNaN(result) )){
            $('.result').text(result);
            $('.curency').html(curency)
        } else {
            $('.result').text('-');
            $('.curency').html(curency)
        }
    }
    moveVSumma();



    //$('#depositSum_value').keypress(function (e) {
    //    var valu=$(this).val();
    //    if (e.which == 13) {
    //        $('#depositSum_slider').val(valu);
    //    }
    //    moveVSumma();
    //});

    //События
    //выбор тарифа
    $('.vklad ').click(function(){
        $('.vklad').removeClass('active');
        $(this).addClass('active');
        moveVSumma();
    });

    //выбор параметров
    $('.onoffswitch-checkbox ').click(function(){
        moveVSumma();
    });

    //Конпка подобрать
    $('.js-maxvklad').click(function(){
        maxProcent();
    });


    // When the slider value changes, update the input and span
    uSlider.noUiSlider.on('update', function( values, handle ) {
        if ( uSlider.value < 91 ) {
            uSlider.value = 91;
        }
        uSlider_val.value = values[handle];
        $('.vDays').text($('#depositPeriod_input').val());
        moveVSumma();
    });

    uSlider_val.addEventListener('change', function(){
        uSlider.noUiSlider.set(this.value);
        moveVSumma();
    });

    // When the slider value changes, update the input and span
    updateSlider.noUiSlider.on('update', function( values, handle ) {
        updateSliderValue.value = values[handle];
        moveVSumma();
    });

    // When the input changes, set the slider value
    updateSliderValue.addEventListener('change', function(){
        updateSlider.noUiSlider.set(this.value);
        moveVSumma();
    });


    function maxProcent(){
        var myArray = localStorage.getItem('procents').split(',');
        var max = myArray[0];
        for (i = 1; i < myArray.length; ++i) {
            if (myArray[i] > max) max = myArray[i];
        }
        var numb=myArray.indexOf(max);
        $('.vklad.dib').eq(numb).trigger('click');
    };
});
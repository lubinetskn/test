function mortgagejs(){
    $('.b-form-mortgage-item-info input').change(function(){
            if ($(this).attr('id')=='monthMoney'){
                move4();
            } else {
                move1();
            }
        }
    );

    $('#myonoffswitch').click(function(){
        move1();
    });

    $('.b-form-mortgage-item-info input').keypress(function (e) {
        var valu=$(this).val();
        if (e.which == 13) {
            if ($(this).attr('id')=='monthMoney'){
                console.log('in');
                $('#monthMoney-slider').val(valu);
                move4();
            }
            console.log('out');
            if ($(this).attr('id')=='creditCost'){
                $('#creditCost-slider').val(valu);
                move12();
            }

            if ($(this).attr('id')=='inProcent'){
                $('#inProcent-slider').val(valu);
                move1();
            }

            if ($(this).attr('id')=='flatCost'){
                console.log('in flatCost');
                $('#flatCost-slider').val(valu);
                move1();
            }

            if ($(this).attr('id')=='inPeriod'){
                $('#inPeriod-slider').val(valu);
                move1();
            }

            if ($(this).attr('id')=='firstMoney'){
                $('#firstMoney-slider').val(valu);
                move1();
            }

          //  return false;
        }
    });


    $('#inPeriod-slider').noUiSlider({
        start: 15,
        step: 1,
        format: wNumb({
            decimals: 0
        }),
        range: {
            'min':  1 ,
            'max':  30
        }
    });
    $('#inPeriod-slider').Link('lower').to($('#inPeriod'));

    $('#inProcent-slider').noUiSlider({
        start: 13,
        step: 0.5,
        format: wNumb({
            decimals: 1
        }),
        range: {
            'min':   11.5 ,
            'max':  30
        }
    });
    $('#inProcent-slider').Link('lower').to($('#inProcent'));

    $('#monthMoney-slider').noUiSlider({
        start: 100000,
        format: wNumb({
            decimals: 0
        }),
        range: {
            'min':  3000,
            'max':  1000000
        }
    });
    $('#monthMoney-slider').Link('lower').to($('#monthMoney'));

    $('#flatCost-slider').noUiSlider({
        start: 5000000,
        format: wNumb({
            decimals: 0
        }),
        range: {
            'min':   5000000 ,
            'max':  10000000
        }
    });
    $('#flatCost-slider').Link('lower').to($('#flatCost'));

    $('#firstMoney-slider').noUiSlider({
        start: 30,
        format: wNumb({
            decimals: 0
        }),
        range: {
            'min':   5 ,
            'max':  100
        }
    });
    $('#firstMoney-slider').Link('lower').to($('#firstMoney'));

    $('#creditCost-slider').noUiSlider({
        start: 100000,
        format: wNumb({
            decimals: 0
        }),
        range: {
            'min':   50000 ,
            'max':  10000000
        }
    });
    $('#creditCost-slider').Link('lower').to($('#creditCost'));

    move1();
    //двигаем сумму кредита
    function move1(){
        var srok = $('#inPeriod-slider').val(),
            pervonach = $('#firstMoney-slider').val(),
            summa = $('#creditCost-slider').val(),
            procent=$('#inProcent-slider').val(),
            flatcost=$('#flatCost-slider').val(),
            mes, b, ps, qa;

        summa=flatcost-(flatcost*pervonach/100);
        $('#creditCost-slider').val(summa);
        ps=procent/12/100;
        b=Math.pow(1+ps,-srok*12+2);
        qa=ps/(1-b);
        mes=summa*qa;
        summa=Math.round(summa);
        $('#monthMoney-slider').val(mes);
        if (srok<5) {
          if (srok==1) {
              $('#inPeriod+ label').text('год')
          } else {
              $('#inPeriod+ label').text('года')
          }
        } else {
            $('#inPeriod + label').text('лет')
        }
    }
    function move12(){
        var srok = $('#inPeriod-slider').val(),
            pervonach = $('#firstMoney-slider').val(),
            summa = $('#creditCost-slider').val(),
            procent=$('#inProcent-slider').val(),
            flatcost=$('#flatCost-slider').val(),
            mes, b, ps, qa;

        //summa=flatcost-(summa*pervonach/100);
        //ps=procent/12/100;
        //b=Math.pow(1+ps,-srok*12+2);
        //qa=ps/(1-b);
        //mes=summa*qa;
        //summa=Math.round(summa);

        ps=procent/12/100;
        qa= Math.pow(1+ps,-(srok-2))
        mes= summa * ps / (1-qa)

        $('#monthMoney-slider').val(mes);
        if (srok<5) {
            if (srok==1) {
                $('#inPeriod+ label').text('год')
            } else {
                $('#inPeriod+ label').text('года')
            }
        } else {
            $('#inPeriod + label').text('лет')
        }
    }
    $('#creditCost-slider').on('slide', move12);
    $('#inPeriod-slider').on('slide', move1);
    $('#firstMoney-slider').on('slide', move1);
    $('#inProcent-slider').on('slide', move1);
    $('#flatCost-slider').on('slide', move1);

    //двигаем ежемесячный
    function move4(){
        var srok,
            pervonach = $('#firstMoney-slider').val(),
            summa = $('#creditCost-slider').val(),
            mes= $('#monthMoney-slider').val(),
            procent=$('#inProcent-slider').val(),
            flatcost=$('#flatCost-slider').val(),
            ps, x,z;
        summa=flatcost-(flatcost*pervonach/100);
        summa=Math.round(summa);
        ps=procent/12/100;
        z=1-(summa*ps/mes);
        x=1+ps;
        srok=2-(Math.log(z)/Math.log(x));
        $('#inPeriod-slider').val(srok);
    }
    $('#monthMoney-slider').on('slide', move4);

});
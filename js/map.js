var gmarkers1 = [];
var infowindow = new google.maps.InfoWindow({
    content: ''
});

/**
 * Function to init map
 */

function initialize() {
    var center = new google.maps.LatLng(55.7614807, 37.6319394);
    var mapOptions = {
        zoom: 12,
        center: center,
        scrollwheel: false,
        styles: [{
            "featureType": "landscape",
            "stylers": [{"saturation": -100}, {"lightness": 65}, {"visibility": "on"}]
        }, {
            "featureType": "poi",
            "stylers": [{"saturation": -100}, {"lightness": 51}, {"visibility": "simplified"}]
        }, {
            "featureType": "road.highway",
            "stylers": [{"saturation": -100}, {"visibility": "simplified"}]
        }, {
            "featureType": "road.arterial",
            "stylers": [{"saturation": -100}, {"lightness": 30}, {"visibility": "on"}]
        }, {
            "featureType": "road.local",
            "stylers": [{"saturation": -100}, {"lightness": 40}, {"visibility": "on"}]
        }, {
            "featureType": "transit",
            "stylers": [{"saturation": -100}, {"visibility": "simplified"}]
        }, {"featureType": "administrative.province", "stylers": [{"visibility": "off"}]}, {
            "featureType": "water",
            "elementType": "labels",
            "stylers": [{"visibility": "on"}, {"lightness": -25}, {"saturation": -100}]
        }, {
            "featureType": "water",
            "elementType": "geometry",
            "stylers": [{"hue": "#ffff00"}, {"lightness": -25}, {"saturation": -97}]
        }],
        mapTypeId: google.maps.MapTypeId.TERRAIN
    };
    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
    for (i = 0; i < markers1.length; i++) {
        addMarker(markers1[i]);
    }

}

/**
 * Function to add marker to map
 */

function addMarker(marker) {
    var category = marker[4];
    var title = marker[1];
    var pos = new google.maps.LatLng(marker[2], marker[3]);
    var content = marker[1];
    var image = {
        url: ' /netcat_template/template/2/blocks/images/marker_44x30.png',
    //    url: './blocks/images/marker_44x30.png',
        // This marker is 20 pixels wide by 32 pixels tall.
        size: new google.maps.Size(30, 44),
        // The origin for this image is 0,0.
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at 0,32.
        anchor: new google.maps.Point(15, 44)
    };

    //IE не поддерживает svg  в иконках для остальных добавляем
    function msieversion() {

        var ua = window.navigator.userAgent;
        var msie = ua.indexOf("MSIE ");

        if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./) || !!navigator.userAgent.match(/Edge/))      // If Internet Explorer, return version number
          image.url='/netcat_template/template/2/blocks/images/marker_44x30.png';
       //   image.url='./blocks/images/marker_44x30.png';
        else                 // If another browser, return 0
          image.url='/netcat_template/template/2/blocks/images/svg/marker_44x30.svg';
       //   image.url='./blocks/images/svg/marker_44x30.svg';

        return false;
    };
    msieversion();


    marker1 = new google.maps.Marker({
        title: title,
        position: pos,
        category: category,
        icon: image,
        map: map
    });

    gmarkers1.push(marker1);

    // Marker click listener
    google.maps.event.addListener(marker1, 'click', (function (marker1, content) {
        return function () {
            infowindow.setContent(content);
            infowindow.open(map, marker1);
            map.panTo(this.getPosition());
            map.setZoom(15);
        }
    })(marker1, content));
}

/**
 * Function to filter markers by category
 */

filterMarkers = function (category) {
    for (i = 0; i < markers1.length; i++) {
        marker = gmarkers1[i];
        // If is same category or category not picked
        if (marker.category == category || category.length === 0) {
            marker.setVisible(true);
        }
        // Categories don't match 
        else {
            marker.setVisible(false);
        }
    }
};


// Init map
initialize();

function checkHash() {
    $( "body" ).scrollTop( 0 );
    var hashTab = window.location.hash;
    if (hashTab == "") hashTab = "#tab1";
    $('a[href="' + hashTab + '"]').trigger('click');
}

setTimeout(checkHash, 100);

$('.b-checkers-item').click(function (e) {

    e.preventDefault();
    $('.btabs').removeClass('active');
    var hashTab = $(this).attr('href');
    location.href=location.href.split('#')[0]+hashTab;
  //history.pushState(null, null, hashTab);
    $(hashTab).addClass('active');
    $('.b-checkers-item').removeClass('active');
    $(this).addClass('active');
    var kinda = $(this).data('kind');
    filterMarkers(kinda);

});

$('.map-marker-name').click(function () {

    var d_title=$(this).data('firstline'),
        d_subtitle=$(this).data('adress'),
        d_tel=$(this).data('tel'),
        d_rasp=$(this).data('rasp'),
        d_text=$(this).data('text'),
        mc=$(this).data('mc'),
        html,
        data;

        for (var i = 0; i < markers1.length; i++) {
            if (markers1[i][0] == $(this).data('markerd')) {
                data= markers1[i];
                var laLatLng = new google.maps.LatLng( data[2],  data[3]);
                map.panTo(laLatLng);
                map.setZoom( 14 );
            }
        }



    if (d_title!=undefined) {
        d_title = '<div class="b-map-details-title">' + d_title + '</div>';
    } else{
        d_title=''
    }
    if (d_subtitle!=undefined) {
        d_subtitle='<div class="b-map-details-subtitle">'+d_subtitle+'</div>';
    } else{
        d_subtitle=''
    }
    if (d_text!=undefined) {
        d_text= '<div class="b-map-details-text">'+d_text+'</div>';
    } else{
        d_text=''
    }
    if (d_tel!=undefined) {

        if (mc==undefined) {
            mc= '';
        }
        d_tel='<i class="tel-ico"></i><div class="b-map-details-tel"><span>'+d_tel+'</span><br>'+mc+'</div>';
    } else{
        d_tel=''
    }
    if (d_rasp!=undefined) {
        d_rasp='<i class="time-ico"></i><div class="b-map-details-subtext">'+d_rasp+'</div>'
    } else{
        d_rasp=''
    }
    html='<div class="b-map-details-close"></div>'+d_title+d_subtitle+d_text+d_tel+d_rasp;
    $('.b-map-details').html(html);
    if ( screen.width>376){
        $('.map-wrap').addClass('showmore');
    }


});

$('section').on('click', '.b-map-details-close, .b-map-details-arr',function () {
    if (screen.width>376) {
        $('.map-wrap').removeClass('showmore');
    }
});

$( document ).ready(function(){

    if ( location.pathname=='/'){

        var scene = document.getElementById('scheene');
        var parallax = new Parallax(scene);

    }
    var buttonUslugi = $('#b-uslugi');
    buttonUslugi.click(function(){
        $('.b-service').toggleClass('m-min');
        buttonUslugi.toggleClass('m-uparrow');
        if ($(".2blocks").hasClass('m-min')){
            buttonUslugi.text("Больше услуг");
        } else {
            buttonUslugi.text("Cвернуть");
        }
    });


   // var timerId = setTimeout(timetoChange, 5000);

    function timetoChange(){
        $('.b-main-slider-buttonright').trigger('click');
    }

    $('.b-main-slider-buttonleft').click(function(){ //предыдущий слайд
        $('.b-main-slider-item').removeClass('active').eq(2).addClass('active');
        var first=$('.b-main-slider-item').eq(0);
        $('.b-main-slider-list').append(first);
    });
    $('.b-main-slider-buttonright').click(function(){ //следующий слайд
        $('.b-main-slider-item').removeClass('active').eq(0).addClass('active');
        var last=$('.b-main-slider-item').eq(2);
        $('.b-main-slider-list').prepend(last);
    });

    $(window).scroll(function(){
        $('.b-advantages-picture').each(function(){
            var imagePos = $(this).offset().top;
            var TopOffWindow = $(window).scrollTop();
            if (imagePos < TopOffWindow +300){
                $(this).addClass('_state_run');
            }
            if (imagePos > TopOffWindow +400){
                $(this).removeClass('_state_run');
            }
        });
        $('.b-advantages-picture').hover(function() {
            $(this).removeClass('_state_run');
            $(this).addClass('_state_run');
        }, function() {
            $(this).removeClass('_state_run');
        });
    });
});
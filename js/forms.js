var current_fs, next_fs, previous_fs;

$(".action-button").click(function(){
    if ($(this).hasClass('next')){
        checkI();
        if (!$(this).hasClass('b-disabled')) {
            current_fs = $(this).parent();
            next_fs = $(this).parent().next();
            current_fs.removeClass('active').hide();
            next_fs.addClass('active').show();
        }
    };

    if ($(this).hasClass('previous')){
        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();
        current_fs.removeClass('active').hide();
        previous_fs.addClass('active').show();
    };

    if (!$(this).hasClass('submit')){
        return false;
    }
});

function checkI(){
    var i= 0,
        j= 0,
        t= 0,
        k= 0;

    if ($('#superForma fieldset').eq(0).hasClass('active')){
        if ($('#f_fio').val()!=''){
            i++;
            $('#f_fio').closest('div').removeClass('notFull');
        } else {
            $('#f_fio').closest('div').addClass('notFull');
        }
        if ($('#f_mobphone').val()!=''){
            i++;
            $('#f_mobphone').closest('div').removeClass('notFull');
        } else {
            $('#f_mobphone').closest('div').addClass('notFull');
        }
        if (typeof ( $('input[name=f_currency]:checked').val() ) != 'undefined') {
            i++;
            $('.b-inline').removeClass('notFull');
        } else {
            $('.b-inline').addClass('notFull');
        }

        if ($('#aggryment').prop('checked')!=''){
            i++;
            $('#aggryment').closest('.checkers').removeClass('notFull');
        } else {
            $('#aggryment').closest('.checkers').addClass('notFull');
        }

        if ($('#f_email').val()!=''){
            i++;
            $('#f_email').closest('div').removeClass('notFull');
        } else {
            $('#f_email').closest('div').addClass('notFull');
        }

        if (i==5){
            $('.js-tosecondstep').removeClass('b-disabled')
        } else {
            $('.js-tosecondstep').addClass('b-disabled')
        }
    }

    if ($('#superForma fieldset').eq(1).hasClass('active')) {
        if ($('#f_sernum').val() != '') {
            j++;
            $('#f_sernum').closest('div').removeClass('notFull');
        } else {
            $('#f_sernum').closest('div').addClass('notFull');
        }
        if ($('#f_dateext').val() != '') {
            j++;
            $('#f_dateext').closest('div').removeClass('notFull');
        } else {
            $('#f_dateext').closest('div').addClass('notFull');
        }
        if ($('#f_camvydan').val() != '') {
            j++;
            $('#f_camvydan').closest('div').removeClass('notFull');
        } else {
            $('#f_camvydan').closest('div').addClass('notFull');
        }
        if ($('#f_cod').val() != '') {
            j++;
            $('#f_cod').closest('div').removeClass('notFull');
        } else {
            $('#f_cod').closest('div').addClass('notFull');
        }
        if ($('#f_dob').val() != '') {
            j++;
            $('#f_dob').closest('div').removeClass('notFull');
        } else {
            $('#f_dob').closest('div').addClass('notFull');
        }
        if ($('#f_plob').val() != '') {
            j++;
            $('#f_plob').closest('div').removeClass('notFull');
        } else {
            $('#f_plob').closest('div').addClass('notFull');
        }

        if (j == 6) {
            $('.js-tothirdstep').removeClass('b-disabled')
        } else {
            $('.js-tothirdstep').addClass('b-disabled')
        }
    }


    if ($('#superForma fieldset').eq(2).hasClass('active')) {
        if ($('#f_index').val() != '') {
            k++;
            $('#f_index').closest('div').removeClass('notFull');
        } else {
            $('#f_index').closest('div').addClass('notFull');
        }
        if ($('#f-region').val() != '') {
            k++;
            $('#f-region').closest('div').removeClass('notFull');
        } else {
            $('#f-region').closest('div').addClass('notFull');
        }

        if (k==2){
            $('.js-tofourthstep').removeClass('b-disabled')
        } else {
            $('.js-tofourthstep').addClass('b-disabled')
        }
    }

    if ($('#superForma fieldset').eq(3).hasClass('active')) {
        console.log(1)
        if ($('#f_password').val() != '') {
            console.log('2'+t)
            t++;
            $('#f_password').closest('div').removeClass('notFull');
        } else {
            $('#f_password').closest('div').addClass('notFull');
        }
        console.log('11'+t)

        if (t==1){
            $('.js-send').removeClass('b-disabled')
        } else {
            $('.js-send').addClass('b-disabled')
        }
    }
}

//$('#superForma fieldset').eq(0).find('input').change(function(){


//Если адресс регистрации соответсвует фактическому скрываем
$('#soot').click(function(){
    if ($('#soot').prop('checked')){
        $('.fact').hide();
    } else {
        $('.fact').show();
    }
});


//Информация о работе
$('#f_work').change(function(){
    if ($('#f_work').val()==1){
        $('#f_nameorg').show();
        $('#f_profess').show();
    }
    if ($('#f_work').val()==2){
        $('#f_nameorg').show();
        $('#f_profess').hide();
    }
    if ($('#f_work').val()==3){
        $('#f_nameorg').hide();
        $('#f_profess').hide();
    }
});


//маски
$("#f_mobphone").inputmask("+7(999)999-99-99");
$("#f_workphone").inputmask("999-9999999");
$("#f_sernum").inputmask("9999 999999");
$("#f_cod").inputmask("999-999");
$("#f_inn").inputmask("999-999-999 99");
$("#f_index").inputmask("999999");
$("#f_pindex").inputmask("999999");
$("#f_dob").inputmask("99.99.9999");
$("#f_dateext").inputmask("99.99.9999");

//обнуление textarea
$("body").on('click', '#f_nameorg', function() {
    if( $('#f_nameorg').text() == "Название организации" ) {
        $('#f_nameorg').text("");
    }
});

$("body").on('blur','#f_nameorg', function() {
    if ( $('#f_nameorg').text() == "" ) {
        $('#f_nameorg').text("Название организации");
    }
});

$("body").on('focus', '#f_camvydan', function() {
    if( $('#f_camvydan').text() == "Кем выдан*" ) {
        $('#f_camvydan').text("");
    }
});

$("body").on('blur','#f_camvydan', function() {
    if ( $('#f_camvydan').text() == "" ) {
        $('#f_camvydan').text("Кем выдан*");
    }
});

$( document ).ready(function(){

    var office = localStorage.getItem("ls_office");

    if (location.pathname=="/otdeleniya/") {
        if (office!=''){
            setTimeout(function(){
                $('.map-marker-name[data-markerd='+office+']').trigger('click');
            },1000);
            localStorage.setItem("ls_office", '');
        }
    } else {
        localStorage.setItem("ls_office", '');
    }

    $('.footer-otdlink').click(function(){

        var ls_id=$(this).data('markd');
        localStorage.setItem("ls_office", ls_id);

        if (location.pathname=="/otdeleniya/") {
            $('.b-checkers-item[data-kind="otd"]').trigger('click');
            $('.map-marker-name[data-markerd='+ls_id+']').trigger('click');
        }
    });

    $('.b-form-mortgage-item-protect').click(function(){
        $(this).toggleClass('active');
    });

    var allbankEvent = $('.m-allbank');
    allbankEvent.click(function(){

        //IE
        function ie9version() {

            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");

            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./) || !!navigator.userAgent.match(/Edge/))      // If Internet Explorer, return version number
                $('.p-nav').toggleClass('showinie');
            else
                $('.p-nav').toggleClass('show');

            return false;
        };

        ie9version();

        if ($('#layerclose').length){
            $('#layerclose').remove();
        } else {
            $('body').prepend('<div id="layerclose"></div>');
        }
    });

});

$(function(){
    var wrapper = $( ".file_upload" ),
        inp = wrapper.find( "input" ),
        btn = wrapper.find( "button" ),
        lbl = wrapper.find( "div" );

    btn.focus(function(){
        inp.focus()
    });
    // Crutches for the :focus style:
    inp.focus(function(){
        wrapper.addClass( "focus" );
    }).blur(function(){
        wrapper.removeClass( "focus" );
    });

    var file_api = ( window.File && window.FileReader && window.FileList && window.Blob ) ? true : false;

    inp.change(function(){
        var file_name;
        if( file_api && inp[ 0 ].files[ 0 ] )
            file_name = inp[ 0 ].files[ 0 ].name;
        else
            file_name = inp.val().replace( "C:\\fakepath\\", '' );

        if( ! file_name.length )
            return;

        if( lbl.is( ":visible" ) ){
            lbl.text( file_name );
            btn.text( "Выбрать" );
        }else
            btn.text( file_name );
    }).change();

});
$( window ).resize(function(){
    $( ".file_upload input" ).triggerHandler( "change" );
});

$('.js-image').click(function(){
    var src=$(this).attr('src'),
        html = '<div id="overlay"><div class="imagewrap"><div class="close-overlay"><i class="fa fa-times"></i></div><img src="'+src+'" alt="" class="js-image"><div></div></div></div>'
    $('body').append(html).show();
});

$('.tooltipik').click(function(){
    var text=$(this).data('tooltip'),
        html = '<div id="overlay"><div class="imagewrap"><div class="close-overlay"><i class="fa fa-times"></i></div><p class="justext">'+text+'</p><div></div></div></div>'
    $('body').append(html).show();
});

$('body').on('click', '.close-overlay',function(){
    $('#overlay').remove();
});

$('body').on('click', '#overlay',function(){
    $('#overlay').remove();
});

// this is the id of the form
$("#superForma").submit(function() {
    var url = $('#superForma').attr('action'); // the script where you handle the form input.

    $.ajax({
        type: "POST",
        url: url,
        data: $("#superForma").serialize(), // serializes the form's elements.
        success: function(data)
        {
            var t=0;

            if ($('#superForma fieldset').eq(3).hasClass('active')) {
                console.log(1)
                if ($('#f_password').val() != '') {
                    console.log('2'+t)
                    t++;
                    $('#f_password').closest('div').removeClass('notFull');
                } else {
                    $('#f_password').closest('div').addClass('notFull');
                }
                console.log('11'+t)

                if (t==1){
                    $('.js-send').removeClass('b-disabled')
                    $('#superForma').remove(); // show response from the php script.
                    $('.yeah').show();
                } else {
                    $('.js-send').addClass('b-disabled')
                }
            }

        },
        error: function(){
            console.log('Error');
        }
    });

    return false; // avoid to execute the actual submit of the form.
});

$('.pleaseUp').click(function(){
    $('.b-orders').slideDown();
    $('body').prepend('<div id="layerclose"></div>');
});

$('body').on('click', '#layerclose', function(){

    $('.b-orders').slideUp();
    $('.p-nav').slideUp(function(){
        $('.p-nav').removeClass('show');
    });
    $('#layerclose').remove();
});


$('.b-orders-close, .js-order-close').click(function(){
    $('.b-orders').slideUp();
    $('#layerclose').remove();
});

$('.b-enter-link a').click(function(){
    $('.b-enter-link a').removeClass('active');
    $(this).addClass('active');
    return false;
});
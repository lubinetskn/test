//var _reportList = $(".b-report-table");
//
$('.report-hidden').hide();

$("body").on("click", ".load_more_report", function(e){
    e.preventDefault();
    $(this).closest('.b-report-table-row').siblings('.report-hidden').slideDown();
    $(this).text('Свернуть').addClass('m-uparrow');
    $('.b-button').removeClass('load_more_report');
    $('.b-button').addClass('remove_report');
});

$("body").on("click", ".remove_report", function(e){
    e.preventDefault();
    $(this).closest('.b-report-table-row').siblings('.report-hidden').slideUp();

    if ($(this).hasClass('report-fin')) {
        $(this).text('Публикуемые формы отчетности ОАО МПБ за предыдущий период').removeClass('m-uparrow');
    } else {
        $(this).text('Архив').removeClass('m-uparrow');
    }

    $('.b-button').removeClass('remove_report');
    $('.b-button').addClass('load_more_report');
});

